
const express = require('express')
const app = express()
const port = 3000

const sha = process.env.GIT_COMMIT
const tiempo = process.env.TIME

app.get('/', (req, res) => {

  res.send("Hola mundo" + "\n" + "v. " + tiempo + "/" + sha)
})

app.listen(port)

module.exports=app


