const app = require("./app")
const supertest = require("supertest")(app)

test("debe devolver un 200", async () => {
    const response = await supertest.get("/")
    expect(response.statusCode).toBe(200)
})
